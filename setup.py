import json
from setuptools import setup, find_packages


def get_version():
    with open('version.json', 'r') as f:
        version_data = json.load(f)
        return version_data['release']['tag_name']


with open('README.md', 'r') as fh:
    long_description = fh.read()

setup(
    name='aws-terraform-state-bootstrap',
    description='AWS S3 Bucket and DynamoDB Lock Table for Terraform Backend State configuration.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/tauri-it/rite/aws-terraform-state-bootstrap',
    author='Tauri-IT, LLC',
    license='Apache 2.0',
    version=get_version(),
    packages=find_packages(),
    install_requires=[
        'click',
        'boto3',
    ],
    entry_points={
        'console_scripts': [
            'terraform-bootstrap = bootstrap.cli:cli',
        ],
    },
)
