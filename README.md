# AWS Terraform State Bootstrap CLI

## Overview
The AWS Terraform State Bootstrap CLI is a command-line tool designed to simplify the setup of Terraform backend state
infrastructure on AWS. It provides a set of commands to create key resources like S3 buckets for storing Terraform
state files and DynamoDB tables for state locking. Additionally, users have precise control over S3 bucket features,
enabling them to easily toggle options such as versioning, analytics, public access block, and lifecycle configuration
based on project needs.

## Features
- **Automated Resource Creation**: Quickly create S3 buckets and DynamoDB tables with default settings or customize them using command-line options.
- **Customizable S3 Buckets**:
  - Enable or disable bucket versioning.
  - Optionally turn on bucket analytics.
  - Configure public access block for security.
  - Add lifecycle configurations for bucket objects.
- **Error Handling**: Gracefully handle errors such as existing resources to prevent accidental modifications or overwrites.
- **Access Analyzer Integration**: Enable Access Analyzer to analyze S3 bucket access for potential security issues.
- **Master Command**: Use a single `create-all` command to provision all required resources.
- **Command-Line Interface**: Simple and intuitive interface powered by Click for user-friendly operations.

---

## Installation
To install the Terraform Backend State Bootstrap CLI from GitLab, follow these steps:

### Clone the repo
1. Clone the repository:
    ```bash
    git clone https://gitlab.com/tauri-it/rite/aws-terraform-state-bootstrap.git
    ```

2. Navigate to the project directory:
    ```bash
    cd aws-terraform-state-bootstrap
    ```

3. Install the CLI using pip:
    ```bash
    pip install .
    ```

### GitLab Package Registry
To install the Terraform Backend State Bootstrap CLI from the GitLab Package Registry, you can install it using `pip` with the appropriate GitLab package URL.
```bash
pip install aws-terraform-state-bootstrap --index-url https://gitlab.com/api/v4/projects/57847296/packages/pypi/simple
```

Additional instructions on download and setup can be found [here](https://gitlab.com/tauri-it/rite/aws-terraform-state-bootstrap/-/packages/25473812).

---

## Usage
After installing the CLI, use the commands below to bootstrap Terraform backend state resources.

### Command Overview
```bash
Options:
  -c, --config TEXT  Path to the config file (default is `configs/config.env.json`)
  -r, --region TEXT  Default working region
  --help             Show this message and exit.

Commands:
  create-all             Master command to create both S3 buckets and...
  create-dynamodb-table  Command to create a DynamoDB lock table.
  create-s3-bucket       Command to create an S3 bucket to store Terraform state files.
```

### Example Usages

1. **Create an S3 Bucket for Terraform State**:
   ```bash
   terraform-bootstrap create-s3-bucket --bucket-name my-terraform-state-bucket
   ```

   Additional Options for `create-s3-bucket`:
  - `--versioning`: Enable or disable bucket versioning (default: enabled).
  - `--analytics`: Enable or disable analytics configuration (default: disabled).
  - `--public-access-block`: Enable or disable public access block for the bucket (default: enabled).
  - `--lifecycle`: Enable or disable lifecycle configuration for the bucket (default: disabled).

   Example:
   ```bash
   terraform-bootstrap create-s3-bucket --bucket-name my-tf-bucket --versioning False --analytics True
   ```

---

2. **Create a DynamoDB Table for State Locking**:
   ```bash
   terraform-bootstrap create-dynamodb-table --table-name my-terraform-lock-table
   ```

---

3. **Master Command to Create All Resources**:
   ```bash
   terraform-bootstrap create-all
   ```

   The `create-all` command uses default settings from the `configs/config.env.json` file. If you want to override the bucket or table names, you can pass them as options:
   ```bash
   terraform-bootstrap create-all --bucket-name my-custom-bucket --table-name my-custom-table
   ```

---

## Configuration
### Default Configuration File
The CLI uses a configuration file (`configs/config.env.json`) to streamline resource setup. This file allows you to define:

- **Default S3 Bucket Values**:
  - Bucket name
  - Lifecycle rules
- **Default DynamoDB Table Values**:
  - Table name
- **AWS Region**:
  - Specify the region for resource provisioning (e.g., `us-east-1`).

### Passing a Custom Config File
You can specify a custom config file using the `-c` or `--config` flag:
```bash
terraform-bootstrap create-all --config /path/to/my-config.json
```

### Example Configuration File
```json
{
  "region": "us-east-1",
  "s3": {
    "bucket_name": "my-default-bucket",
    "lifecycle_configuration_rules": [
      {
        "ID": "MoveToGlacier",
        "Filter": {"Prefix": ""},
        "Status": "Enabled",
        "Transitions": [{"Days": 30, "StorageClass": "GLACIER"}]
      }
    ]
  },
  "dynamodb": {
    "table_name": "my-default-dynamodb-table"
  }
}
```

---

## Advanced Topics

### Access Analyzer
The CLI integrates with AWS's Access Analyzer to enhance security for S3 buckets. If enabled (`analytics` option in `create-s3-bucket`), 
Access Analyzer will help detect overly permissive bucket policies.

To manually enable Access Analyzer:
```bash
terraform-bootstrap create-s3-bucket --analytics True
```

---

## Error Handling
- **Resource Exists**: The CLI will notify you if a bucket or table already exists and is owned by you, preventing unintended overwrites.
- **Region Issues**: Misconfigured or unavailable AWS regions will result in clear error messages.
- **AWS API Failure**: Any AWS-specific errors from the S3 or DynamoDB API will be logged for troubleshooting.

---

## License
This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details.

## Authors
This project is authored by:
- Tauri-IT
