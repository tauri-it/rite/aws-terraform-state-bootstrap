import json


class ConfigEnv:
    def __init__(self, file_path='config.env.json'):
        self.file_path = file_path
        self.config = self._load_config()

    def _load_config(self):
        try:
            with open(self.file_path, 'r') as file:
                config_data = json.load(file)

                return config_data

        except FileNotFoundError:
            print(f"Config file '{self.file_path}' not found.")
            return {}

    def get_value(self, key: str, default: str = None):
        """
        Get the value of a key from the config file.
        :param key: The key whose value is to be retrieved.
        :param default: Default value to return if key not found.
        :return: The value corresponding to the key or the default value if key not found.
        """
        return self.config.get(key, default)

    def get_resource_value(self, resource: str, key: str, default: str = None):
        """
        Get the value of a key from the config file for a specific resource.
        :param resource: The resource name (e.g., 's3', 'dynamodb').
        :param key: The key whose value is to be retrieved.
        :param default: Default value to return if key not found.
        :return: The value corresponding to the key or the default value if key not found.
        """
        if resource in self.config:
            return self.config[resource].get(key, default)
        else:
            return default
