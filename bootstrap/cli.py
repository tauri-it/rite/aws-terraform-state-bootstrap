import click
import boto3
from os import getenv
from botocore.exceptions import ClientError
from pathlib import Path
from bootstrap.config_reader import ConfigEnv


@click.group()
@click.option('--config', '-c', default='configs/config.env.json', help='Path to the config file')
@click.option('--region', '-r', help='Default working region')
@click.pass_context
def cli(ctx: object, config: str, region: str = None):
    if ctx.obj is None:
        ctx.obj = {}
    ctx.obj['CONFIG'] = ConfigEnv(file_path=Path(config))
    ctx.obj['REGION'] = (
        region or
        getenv('AWS_REGION') or
        ctx.obj['CONFIG'].get_value('region')
    )

    if not ctx.obj['REGION']:
        click.echo("Error: No AWS region defined. Set it via CLI, AWS_REGION environment variable, or in the config.")
        ctx.exit(1)


@cli.command()
@click.option('--bucket-name', '-b', help='Name of the S3 bucket')
@click.option(
    '--versioning', default=True, help='Enable or disable bucket versioning. Default is True.'
)
@click.option(
    '--analytics',
    default=False,
    help='Enable or disable bucket analytics configuration. Default is False.',
)
@click.option(
    '--public-access-block',
    default=True,
    help='Enable or disable public access block configuration. Default is True.',
)
@click.option(
    '--lifecycle',
    default=False,
    help='Enable or disable bucket lifecycle configuration. '
         'If Enabled, you will need to configure the `lifecycle_configuration_rules` '
         'in the config.env.json (or equivalent). Default is False.',
)
@click.pass_context
def create_s3_bucket(
    ctx: object,
    versioning: bool,
    analytics: bool,
    public_access_block: bool,
    lifecycle: bool,
    bucket_name: str = None,
):
    """
    Click context object for creating an S3 Terraform Backend State Bucket.

    :param ctx:
    :param bucket_name: Name of the S3 bucket.
    :param versioning: Enable or disable bucket versioning. Default is True.
    :param analytics: Enable or disable bucket analytics configuration. Default is True.
    :param public_access_block: Enable or disable public access block configuration. Default is True.
    :param lifecycle: Enable or disable bucket lifecycle configuration. Default is False.
    :return:
    """
    config = ctx.obj['CONFIG']
    bucket_name = bucket_name or config.get_resource_value('s3', 'bucket_name')
    region = ctx.obj['REGION']

    try:
        s3 = boto3.client('s3', region_name=region)

        # Create the S3 bucket
        if region == 'us-east-1':
            s3.create_bucket(Bucket=bucket_name)
        else:
            s3.create_bucket(
                Bucket=bucket_name, CreateBucketConfiguration={'LocationConstraint': region}
            )
        click.echo(f"S3 bucket '{bucket_name}' created successfully in region {region}.")

        if versioning:
            s3.put_bucket_versioning(
                Bucket=bucket_name, VersioningConfiguration={'Status': 'Enabled'}
            )
            click.echo(f"Versioning enabled for bucket '{bucket_name}'.")

        if analytics:
            enable_access_analyzer(region=region)

            s3.put_bucket_analytics_configuration(
                Bucket=bucket_name,
                Id=f'{bucket_name}-bucket-analytics',
                AnalyticsConfiguration={
                    'Id': f'{bucket_name}-bucket-analytics',
                    'StorageClassAnalysis': {
                        'DataExport': {
                            'OutputSchemaVersion': 'V_1',
                            'Destination': {
                                'S3BucketDestination': {
                                    'Bucket': f'{bucket_name}',
                                    'Prefix': '_AWSBucketAnalytics',
                                    'Format': 'json',
                                }
                            },
                        }
                    },
                },
            )
            click.echo(f"Analytics configuration enabled for bucket '{bucket_name}'.")

        if public_access_block:
            s3.put_public_access_block(
                Bucket=bucket_name,
                PublicAccessBlockConfiguration={
                    'BlockPublicAcls': True,
                    'IgnorePublicAcls': True,
                    'BlockPublicPolicy': True,
                    'RestrictPublicBuckets': True,
                },
            )
            click.echo(f"Public access block configuration enabled for bucket '{bucket_name}'.")

        if lifecycle:
            s3.put_bucket_lifecycle_configuration(
                Bucket=bucket_name,
                LifecycleConfiguration={
                    'Rules': config.get_resource_value('s3', 'lifecycle_configuration_rules')
                },
            )
            click.echo(f"Lifecycle configuration enabled for bucket '{bucket_name}'.")

    except ClientError as e:
        if e.response['Error']['Code'] == 'BucketAlreadyOwnedByYou':
            click.echo(f"The bucket '{bucket_name}' already exists and is owned by you.")
        elif e.response['Error']['Code'] == 'BucketAlreadyExists':
            click.echo(f"The bucket '{bucket_name}' already exists.")
        elif e.response['Error']['Code'] == 'InvalidLocationConstraint':
            click.echo(f'The region {region} is invalid for this request.')
        else:
            click.echo(f'An error occurred: {e}')


@cli.command()
@click.option('--table-name', '-t', help='Name of the DynamoDB Table')
@click.pass_context
def create_dynamodb_table(ctx: object, table_name: str = None):
    """
    Click context object for creating a DynamoDB Terraform Backend State lock table.

    :param ctx:
    :param table_name: Name of the DynamoDB table.
    :return:
    """
    config = ctx.obj['CONFIG']

    try:
        table_name = table_name or config.get_resource_value('dynamodb', 'table_name')
        region = ctx.obj['REGION']

        # create a DynamoDB table
        dynamodb = boto3.client('dynamodb', region_name=region)
        dynamodb.create_table(
            TableName=table_name,
            KeySchema=[{'AttributeName': 'LockID', 'KeyType': 'HASH'}],  # Partition key
            AttributeDefinitions=[{'AttributeName': 'LockID', 'AttributeType': 'S'}],  # String
            ProvisionedThroughput={
                'ReadCapacityUnits': 2,  # TODO - make this adjustable
                'WriteCapacityUnits': 2,  # TODO - make this adjustable
            },
        )
        click.echo(f"DynamoDB table '{table_name}' created successfully in region {region}.")

    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceInUseException':
            click.echo(f"The table '{table_name}' already exists.")
        else:
            click.echo(f'An error occurred: {e}')


@cli.command()
@click.pass_context
def create_all(ctx: object):
    """
    Master command to create both S3 buckets and DynamoDB tables with their default-set values.
    """
    ctx.invoke(create_s3_bucket)
    ctx.invoke(create_dynamodb_table)


def get_aws_caller_identity():
    """
    Get the AWS Caller Identity for the current session.

    :return: AWS partition name (e.g., 'aws', 'aws-cn', 'aws-us-gov') or None if unable to determine,
    and the Account ID.
    """
    try:
        sts = boto3.client('sts')
        response = sts.get_caller_identity()
        account_id = response['Account']
        arn_partition = response['Arn'].split(':')[1]
        output = {'account_id': account_id, 'partition': arn_partition}

        print(f'Partition is {arn_partition} for account {account_id}')

        return output

    except Exception as e:
        print(f'Failed to get AWS partition: {e}')
        return None


def enable_access_analyzer(region: str):
    """
    Enable Access Analyzer for the region.

    :param region: AWS region where the bucket is located.
    :return: True if Access Analyzer is enabled. Creates the analyzer for S3, otherwise.
    """
    try:
        analyzer = boto3.client('accessanalyzer', region_name=region)
        response = analyzer.list_analyzers(type='ACCOUNT')
        analyzers = response['analyzers']

        if len(analyzers) > 0 == 'ACTIVE':
            return True

        analyzer.create_analyzer(
            analyzerName='s3-bucket-analyzer', type='ACCOUNT'
        )  # TODO - make analyzerName configurable
        return True

    except ClientError as e:
        if e.response['Error']['Code'] == 'AccessAnalyzerException' and e.response['Error'][
            'Message'
        ].startswith('The analyzer does not exist'):
            print(f'Access Analyzer does not exist in the region {region}.')
        elif e.response['Error']['Code'] == 'ConflictException':
            click.echo(
                'The Access Analyzer "s3-bucket-analyzer" already exists.'
            )  # TODO - make s3-bucket-analyzer configurable
        else:
            print(f'Failed to enable Access Analyzer: {e}')
        return False


if __name__ == '__main__':
    cli()
